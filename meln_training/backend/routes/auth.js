//include express
const express = require('express')

//include express router
const router = express.Router()

//load jwt for json web token generation/signing
const jwt = require('jsonwebtoken')

//load passport for auth
const passport = require('passport')

//load our custom passport module
const appPassport = require('../passport')

//login user to the system
router.post('/login', (req, res, next) => {
	passport.authenticate('local', {session: false}, (err, user, info) => {
		// if we're unable to validate, request is unauthorized
		if(err || !user) {
			return res.status(400).json({
				'error' : 'something is not right'
			})
		}

		// console.log('the info is ' + info)
		console.log('success')

		req.login(user, {session:false}, (err) => {
			if(err) {
				res.send(err)
			}

			//create a json web token
			//jwt.sign(payload, secretkey, [oprtions, callback])
			const token = jwt.sign(user.toJSON(), 'secret-key', {expiresIn: '45m'})
			// console.log(token)
			return res.status(200).json({
				'data' : {
					'user'  : user,
					'token' : token
				}
			})

		})

	}) (req, res)
})

// Logout
router.get('/logout', function(req, res) {
	console.log('User is logged out')
	req.logout()
	res.json({
		status: 'logout',
		msg: 'pls login'
	})
})



module.exports = router
