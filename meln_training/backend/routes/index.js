const express = require('express');
const router = express.Router();
const UserModel = require('../models/User.js')
const bcrypt = require('bcrypt-nodejs')

router.post('/register', (req, res) => {
  // console.log(req.body)
  let email = req.body.email;
  let password = req.body.password;
  console.log(email);
  console.log(password);
  if(!email || !password) {
    return res.status(500).json({
      "error": "incomplete"
    })
  }

  UserModel.find({'email': email})
  .then((users, err) => {
    // console.log(users);
    if(err){
      return res.status(500).json({
        'error' : 'an error occured'
      })
    }

    if(users.length > 0) {
      return res.status(500).json({
        'error' : 'Email is already taken'
      })
    }

    bcrypt.genSalt(10, function(err,salt){
      // console.log(salt);
      bcrypt.hash(password, salt, null, function(err, hash){
        // console.log(hash);
        let newUser = UserModel({
          'email': req.body.email,
          'password': hash
        })

        // console.log(newUser);
        newUser.save(err => {
          if(!err) {
            return res.json({
              'message': 'User Registered Successfully'
            })
          }
        })
      
      })
    })
  })
})

module.exports = router
