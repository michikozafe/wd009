const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
	email : {
		type : String,
		required : [true, 'Email field is required']
	},
	password : {
		type : String,
		required : [true, 'Password field is required']
	},
  admin : {
    type: Boolean,
    default: false
  }
})

module.exports = mongoose.model('User', UserSchema);
