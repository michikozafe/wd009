// include the express framework
const express = require("express");

// instantiate an express app
const app = express();

const passport = require('passport');
const jwt = require('jsonwebtoken');
require('./passport')

// include cors package to allow cross-origin requests
const cors = require('cors');

// allow cross origin requests
app.use(cors());

// add body parsing to access the data in our request
const bodyParser = require("body-parser");

// include mongoose for handling multiple db queries/connections
const mongoose = require("mongoose");

// establish a connection to our database
const databaseUrl = "mongodb+srv://sample:Password123@cluster0-7xqlh.mongodb.net/meln_b30?retryWrites=true&w=majority";

// establish a connection to the remote db using the connect method of mongoose
mongoose.connect(databaseUrl, { useNewUrlParser: true });

mongoose.connection.once("openURI", () => {
  console.log("Remote database established");
});

// configure express/middleware to use the body parser package in retrieving request bodies
app.use(bodyParser.json());

app.use((err, req, res, next) => {
  res.status(422).send({ error: err.message });
});

// tuitt dev routes
const dev = require("./routes/api.js");
app.use("/devs", passport.authenticate('jwt', { session: false }), dev);

const index = require("./routes/index.js");
app.use("/", index);

const song = require("./routes/song_api.js");
app.use("/songs", song);

const auth = require('./routes/auth.js');
app.use('/auth', auth);

const port = 3000;

app.listen(port, () => {
  // parameter:path where to listen, callback function
  console.log(`server is running at port ${port}`);
});

