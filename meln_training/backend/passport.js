const passport = require('passport');
const passportJWT = require('passport-jwt');
const ExtractJWT = passportJWT.ExtractJwt
const LocalStrategy = require('passport-local').Strategy;
const JWTStrategy = passportJWT.Strategy;
const moment = require('moment');
const bcrypt = require('bcrypt-nodejs');

const UserModel = require('./models/User');

passport.use(new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password'
    },
    function (email, password, done) {
      //this one is typically a DB call. Assume that the returned user object is pre-formatted and ready for storing in JWT
      UserModel.findOne({'email': email})
         .then(user => {
             if (!user) {
                 return done(null, false, {message: 'Invalid Credentials.'});
             }
             if(email === user.email) {
              if(!bcrypt.compareSync(password, user.password)) {
                return done(null, false, {message: 'Invalid Credentials.'});
              }
              return done(null, user)
             }
             return done(null, fasle, {message: 'Invalid Credentials.'});
        })
        .catch(err => done(err));
    } 
));

// configure JWT
passport.use(new JWTStrategy({
  jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
  secretOrKey   : 'secret-key'
},
function (jwtPayload, cb) {
  //find the user in db if needed.
  return UserModel.findOne(jwtPayload.id)
    .then(user => {
      return cb(null, user);
    })
    .catch(err => {
      return cb(err);
    });
}
));