<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    </head>
    <body>
       <button id="logout" class="btn btn-primary">Logout</button>
        <button id="add-dev" class="btn btn-primary btn-block" data-toggle="modal" data-target="#newDev">Create A New Dev</button>
        <div id="success" class="alert text-center"></div>
        
        {{-- Add New Dev Modal --}}
        <div class="modal fade" id="newDev">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Create A New Dev</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        </button>
                    </div>
                    <div class="modal-body">
                        <form id="createDev">
                            @csrf
                            <div class="form-group">
                                <label for="name" class="col-form-label">Name:</label>
                                <input type="text" class="form-control" id="name" placeholder="Juan Dev">
                            </div>
                            <div class="form-group">
                                <label for="portfolio" class="col-form-label">Portfolio:</label>
                                <input type="text" class="form-control" id="portfolio" name="portfolio" placeholder="devportfolio@gmail.com">
                            </div>
                            <div class="form-group">
                                <label for="batch" class="col-form-label">Batch Number:</label>
                                <input type="number" class="form-control" id="batch" name="batch" placeholder="30">
                            </div>
                        </form>
                        <button id="createButton" class="btn btn-primary btn-block" data-dismiss="modal">Create New Dev</button>
                    </div>
                </div>
            </div>
        </div>

        <ul id="devList"></ul>
        <div class="container">
            <div class="row">
                <div class="col-md 8 mx-auto">
                    
                </div>
            </div>
        </div>

        {{-- Registration Form --}}
        <div class="container">
            <div class="row">
              <div class="col-md-8 mx-auto">
                <!-- start form -->
                <form>
                  <h1 class="mb-3 font-weight-normal text-center">
                    Create Your Account
                  </h1>
                  <hr class="divider bg-success" />
                  <!-- name -->
                  <div class="name row">
                    <div class="col-md-6">
                      <label for="fname">First Name</label>
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <div class="input-group-text"><i class="fas fa-id-card"></i></div>
                        </div>
                        <input
                          type="text"
                          id="fname"
                          class="form-control"
                          placeholder="First Name"
                        />
                      </div>
                    </div> 
                    <!-- end col 6 -->
                  </div>
                  <!-- end name -->
    
                  <!-- email -->
                  <label for="email" class="pt-2">Email address</label>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <div class="input-group-text"><i class="fas fa-envelope"></i></div>
                    </div>
                    <input
                      type="email"
                      class="form-control"
                      id="email"
                      placeholder="you@example.com"
                    />
                  </div>
                  
                  <!-- username -->
                  <label for="username" class="pt-2">Username</label>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <div class="input-group-text"><i class="fas fa-id-card"></i></div>
                    </div>
                    <input
                      type="text"
                      class="form-control"
                      id="username"
                      placeholder="Create your username"
                    />
                  </div>
                  
                  <!-- password -->
                  <label for="inputPassword" class="pt-2">Password</label>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <div class="input-group-text"><i class="fas fa-key"></i>
                      </div>
                    </div>
                    <input
                      type="password"
                      class="form-control"
                      id="inputPassword"
                      placeholder="Create a password"
                    />
                  </div>
                  
                  <!-- confirm password -->
                  <label for="confirmPassword" class="pt-2">Confirm Password</label>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <div class="input-group-text"><i class="fas fa-key"></i>
                      </div>
                    </div>
                    <input
                      type="password"
                      id="confirmPassword"
                      class="form-control"
                      placeholder="Confirm your password"
                    />
                  </div>
                </form>
                <!-- end form -->
    
                <!-- Button trigger modal -->
                <button
                  class="btn btn-lg btn-success btn-block"
                  data-toggle="modal"
                  data-target="#signup-btn"
                >
                  Sign Up
                </button>
              </div>
              <!-- end col -->
            </div>
            <!-- end row -->
          </div>
        
        <!-- Edit Modal -->
        <div class="modal fade" id="editModal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                <h5 class="modal-title">Edit Dev</h5>
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                <div class="modal-body">
                    <form id="editDev">

                    </form>
                </div>
            </div>
            </div>
        </div>

        <script>
            document.querySelector('#createButton').addEventListener("click", function(){
                let nameField = document.querySelector('#name').value;
                let portfolio = document.querySelector('#portfolio').value;
                let batch = document.querySelector('#batch').value;
                // console.log(nameField);
                // console.log(portfolio);
                // console.log(batch);

                let formData = new FormData();
                formData.name = nameField
                formData.portfolio = portfolio
                formData.batch = batch
                // console.log(formData);

                fetch("http://localhost:3000/devs/create", {
                    method: 'POST',
                    headers:{
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(formData)
                }).then(res => res.json())
                .then (res => {
                    // console.log(res.name);
                    document.querySelector('#success').innerHTML = `Successfully Created Dev ${res.name}`;
                    document.querySelector('#success').classList.add("alert-success");
                })
                .catch(error => console.error('Error:', error));
            });

            // retrieve devs from db
            // document.querySelector('#devList').innerHTML = "<li class='list-unstyled'>Hello World</li>";
            let token = `Bearer ${localStorage.token}`
            let email = localStorage.email
            let isAdmin = localStorage.admin
            
            const url = 'http://localhost:3000/devs'
            fetch(url, {
                method: 'GET',
                headers:{
                    'Content-Type': 'application/json',
                    'Authorization': token
                },
            }).then(res => res.json())
            .then(data => {
                // console.log(data.data.devs);
                const devs = data.data.devs
                let devGroups = ' ';
                devs.map(dev => {
                    // console.log(dev);
                    devGroups += 
                    `
                    <li class="text-center card p-5 list-unstyled">
                        ${dev.name}
                        <button class="btn btn-warning editButon" data-id="${dev._id}" data-type="edit" data-toggle="modal" data-target="#editModal">Edit</button>
                        <button class="btn btn-danger deleteButton" data-id="${dev._id}" data-type="delete">Delete</button>
                    </li>
                    
                   `
                })
                document.querySelector('#devList').innerHTML = devGroups;
            })

            
            
            // Delete Dev
            document.querySelector('#devList').addEventListener('click', function(e) {
                let id = e.target.getAttribute('data-id')
                let type = e.target.getAttribute('data-type')
                // console.log(e.target.classname);
                // console.log(id)
                // console.log(type)
                if(type === "delete") {
                    // console.log(id);
                    // console.log(e.target.parentNode);
                    let confirmation = confirm('Are you sure you want to delete?')
                    if(!confirmation) {
                        return false
                    }
                    e.target.parentNode.remove();
                    removeDev(id)
                }
            })

            function removeDev(id) {
                // console.log(id);
                const url = 'http://localhost:3000/devs/delete'
                fetch(url, {
                    method: 'DELETE',
                    headers:{
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({'id': id})
                }).then(res => res.json())
                .then(data => {
                    console.log(data);
                })
            }

            // Edit Dev
            document.querySelector('#devList').addEventListener("click", function(e) {
                let editId = e.target.getAttribute('data-id')
                let type = e.target.getAttribute('data-type')
                if(type === "edit") {
                    // console.log(`This is the id ${editId}`);
                    const url = `http://localhost:3000/devs/${editId}`
                    fetch(url)
                    .then(res => res.json())
                    .then(data => {
                        const dev = data.data.dev;
                        // console.log(dev);
                        let devDetails = '';
                        devDetails = `
                            {{ csrf_field() }}
                            <input type="hidden" data-id="${dev._id}" id="editId">
                            <input type="text" name="name" id="editName" placeholder="${dev.name}" required>
                            <input type="text" name="portfolio" id="editPortfolio" placeholder="${dev.portfolio}" required>
                            <input type="number" name="batch" id="editBatch" placeholder="${dev.batch}" min="1" required>
                            <button class="editSub" data-name="editSub" data-dismiss="modal">Submit Changes</button>
                        `
                        document.querySelector('#editDev').innerHTML = devDetails;
                    })
                }
            })

            document.querySelector('#editDev').addEventListener('click', function(e) {
                if(e.target.className === "editSub") {
                    // console.log("Click the button");
                    let name = document.querySelector('#editName').value
                    let portfolio = document.querySelector('#editPortfolio').value
                    let batch = document.querySelector('#editBatch').value

                    let formData = new FormData();

                    formData.name = name
                    formData.portfolio = portfolio
                    formData.batch = batch

                    // console.log(formData);

                    let editId = document.querySelector('#editId').dataset.id
                    // console.log(editId);

                    const url = `http://localhost:3000/devs/${editId}`
                    fetch(url, {
                        method: 'PUT',
                        headers:{
                            'Content-Type': 'application/json'
                        },
                        body: JSON.stringify(formData)
                    }).then(res => {
                        window.location = '/'
                    })
                    .then(data => {
                        console.log(data);
                    })

                }
            })

            document.querySelector('#logout').addEventListener('click', function() {
              if(window.confirm('Do you want to proceed?')) {
                fetch('http://localhost:3000/auth/logout')
                .then(res => {
                  return res.json()
                })
                .then((data) => {
                  localStorage.clear()
                  window.location.replace('/')
                })
                .catch(function(err){
                  console.log(err)
                })
                console.log(localStorage.email);
                console.log(localStorage.isAdmin);
                console.log(localStorage.token);
              }
            })

        </script> 

    </body>
    {{-- Bootstrap Scripts --}}
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</html>
