<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
  <!-- Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">

</head>

<body>
  <div class="container">
    <div class="form-group">
      <input type="email" name="email" id="email" class="form-control" placeholder="Email">
    </div>
    <div class="form-group">
      <input type="password" name="password" id="password" class="form-control" placeholder="Password">
    </div>
    <button class="btn btn-success" id="login">Login</button>
  </div>

  <script>
    document.querySelector('#login').addEventListener('click', function(){
        let email = document.querySelector('#email').value;
        let password = document.querySelector('#password').value;
        // console.log(`email is ${email} password is ${password}`);

        const url = `http://localhost:3000/auth/login`
        fetch(url, {
            method: 'POST',
            headers:{
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
              email: email,
              password: password
            })
        }).then(res => res.json())
        .then(res => {
          // console.log(res.data.token);
          // console.log(res.data.user);
          // console.log(res.data.user.admin);
          localStorage.setItem('token', res.data.token)
          localStorage.setItem('email', res.data.user.email)
          localStorage.setItem('admin', res.data.user.admin)

          window.location = '/'
      })
    })

     

  </script>

</body>
{{-- Bootstrap Scripts --}}
<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

</html>