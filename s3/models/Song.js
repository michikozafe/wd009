// load mongoose library for data modelling
// import mongoose
const mongoose = require("mongoose");

// load mongoose schema for models
const Schema = mongoose.Schema;

// create a new Schema for Devs
const SongSchema = new Schema({
  // name : String,
  title: {
    type: String,
    required: [true, "Title field is required"]
  },
  // portfolio : String,
  genre: {
    type: String,
    required: [true, "Genre field is required"]
  },
  // hired : Boolean,
  length: {
    type: Number,
    default: false
  }
});

// setup the Dev Model and export it to the main app
module.exports = mongoose.model("Song", SongSchema);
