// include the express framework
const express = require("express");

// include the Router used in express framework
const router = express.Router();

// require the dev model
const SongsModel = require("../models/Song");

// router.get('/', (req, res) => {
// res.send({type : 'GET'});
// })

// CREATE A DEV
router.post("/create", (req, res, next) => {
  console.log(req.body);
  // res.send({type: 'POST'});
  SongModel.create(req.body)
    .then(song => {
      res.send(song);
    })
    .catch(next);

  // res.send({
  // type: "POST",
  // name: req.body.name,
  // portfolio: req.body.portfolio,
  // hired: req.body.hired,
  // batch: req.body.batch
  // })
});

// RETRIEVE A DEV
router.get("/", (req, rest) => {
  SongModel.find({}, (err, song) => {
    console.log(song);
    if (!err) {
      return res.json({
        data: {
          'songs': songs
        }
      });
    } else {
      console.log(err);
    }
  });
});

// DELETE A DEV
// localhost:3000/devs/:id
router.delete("/:id", (req, res, next) => {
  // res.send({type: 'DELETE'});
  SongModel.deleteOne({ _id: req.params.id })
    .then(dev => {
      res.send(dev);
    })
    .catch(next);
});

// UPDATE A DEV
router.put("/:id", (req, res, next) => {
  // res.send({type: 'PUT'});
  SongModel.update({ _id: req.params.id }, req.body)
    .then(() => {
      SongModel.findOne({ _id: req.params.id }).then(dev => {
        res.send(dev);
      });
    })
    .catch(next);
});

module.exports = router;
